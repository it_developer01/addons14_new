from unittest import result
from odoo import api , fields , models

class hrms_master_sp(models.Model):
    _name         = 'hrms.master_sp'
    _description  = 'hrms.master_sp'
    name            =fields.Char(string='Nama Sp')
    description     =fields.Char(string='Description')
    is_active       =fields.Boolean()


class hrms_sp_employee(models.Model):

    _name               = 'hrms.sp_employee'
    _description        = 'hrms.sp_employee'
    employee_id         = fields.Many2one(comodel_name='hr.employee',string='Karyawan')
    jenis_sp            = fields.Many2one(comodel_name='hrms.master_sp',string='Jenis pelanggaran',domain = "[('is_active','=','true')]")
    tgl_awal_sp         = fields.Date(string='Tanggal awal sp')
    tgl_akhir_sp        = fields.Date(string='Tanggal Akhir sp')
    tgl_terbit_sp       = fields.Date(string='Tanggal Terbit sp')
    pelanggaran         = fields.Text(string='Pelanggaran')
    potongan            = fields.Float(string='Potongan (%)', default=100.0)
    komponen_potongan   = fields.Many2one(comodel_name='hrms.payroll_component',string="Komponen Potongan")

    @api.onchange('employee_id')
    def _change_employee_ids(self):
        componen_ids_object   = self._getKomponenLine(self.employee_id)
        if componen_ids_object : 
            self.pelanggaran  = componen_ids_object.componen_id.componen_id.ids
            domain      = [('id','in',componen_ids_object.componen_id.componen_id.ids)]
            return {'domain':{'komponen_potongan':domain}}

    def _getKomponenLine(self,employee_ids):
       result = self.env['hrms.master_salary'].search([('employee_id','=',employee_ids.id)])
       return result
    
  





    
