import datetime

from pkg_resources import require
from odoo import api , fields , models

class Overtime(models.Model):
    _name         = 'hrms.form_lembur'
    _description  = 'hrms.form_lembur'
    noform        = fields.Char(string="No Form", readonly=True, required=True, copy=False, default='New')
    tanggal       = fields.Date(
        string='Tanggal Lembur',
        default=fields.Date.context_today,
    )
    divisi       = fields.Many2one(string="Divisi",comodel_name="hr.department",required=True) 
    keterangan   = fields.Selection(
                  string='Keterangan Lembur',
                  required=True,
                  selection=[('0', 'Normal'), ('1', 'Libur Nasional'),('2', 'Istirahat')]
    )
    validasi   = fields.Selection(
                  string='Validasi',
                  required=True,
                  selection=[('0', 'Normal'), ('1', 'Validasi ')]
    )
    formd_ids       = fields.One2many(
        string      ='Detail Lembur',
        comodel_name='hrms.form_lembur_d',
        inverse_name='form_id',
    )

    grandtotal      = fields.Float(String="Grand Total :",
                     default='0.00',compute="_getgrandtotal"
    )
    
    @api.depends('formd_ids')
    def _getgrandtotal(self):
        #self.grandtotal =0
        for recin in self:
            grandtotals =0
            for recordz in recin.formd_ids:
                    grandtotals +=recordz.totalharga
        recin.grandtotal = grandtotals


    @api.model
    def create(self, vals):
     if vals.get('noform', 'New') == 'New':
        vals['noform'] = self.env['ir.sequence'].next_by_code(
            'hrms.form_lembur') or 'New'
     result = super(Overtime, self).create(vals)
     return result


class overtime_d(models.Model):
    _name           ='hrms.form_lembur_d'
    _description    ='hrms.form_lembur_d'

    form_id         =fields.Many2one(comodel_name='hrms.form_lembur',string='form_id')  
    subbagian      =fields.Char(string='Sub Bagian') 
    employee_id     =fields.Many2one(comodel_name='hr.employee',string='Karyawan')
    jam_in          =fields.Char(string='Jam in',default='00.00')
    jam_out         =fields.Char(string='Jam Out',default='00.00')
    uraian          =fields.Text(tring='Uraian Tugas')
    totaljam        =fields.Char(string='Total Jam',compute='_def_count_jam',Store=True)
    harga           =fields.Float(string='Harga')
    totalharga      =fields.Float(string='Total Harga',compute='_def_count_harga',Store=True)

    @api.onchange('form_id.divisi','subbagian','employee_id')
    def get_employee(self):
        divisiidz   =self.form_id.divisi.id  
        emp_object  = self.env['hrms.departement'].search([('parent_departement','=',divisiidz)])
        domain      =[('id','in',emp_object.employes_id.ids)]
        return {'domain' : {'employee_id' : domain}}    

    @api.onchange('employee_id','subbagian')
    def onchange_employee_id(self):
        for rec in self:
            employee         = rec.employee_id
            depobject        = self.env['hrms.departement'].search([('employes_id','=',employee.id)])
            rec.subbagian    = depobject.name

    @api.depends('jam_in','jam_out')
    def _def_count_jam(self):
                for recor in self:
                    if recor.jam_in :
                        jamin   = recor.jam_in
                        timein  =str(jamin)
                        timeinz =timein.split(".")
                        jamins  =float(timeinz[0])
                        if len(timeinz)>1 :
                            menitin =timeinz[1]
                        elif len(timeinz)==1 :
                            menitin ='00'
                        
                        menitinx=menitin[0:2]
                        if len(menitinx) > 1 :
                            menitinx    =round(float((float(menitinx)*6)/10))
                        elif len(menitinx) == 1 :
                            menitinx    = round(float(float(menitinx)*6))

                        jamout  = recor.jam_out
                        timeout =str(jamout)
                        timeoutz=timeout.split(".")
                        jamouts  =float(timeoutz[0])
                        if len(timeoutz)>1 :
                            menitout =timeoutz[1]
                        elif len(timeoutz)==1 :
                            menitout ='00'
                        menitoutx=menitout[0:2]
                        if len(menitoutx) > 1 :
                            menitoutx    =round(float((float(menitoutx)*6)/10))
                        elif len(menitoutx) == 1 :
                            menitoutx    = round(float(float(menitoutx)*6))
                        time_1 = datetime.timedelta(hours= round(jamins) , minutes=menitinx)
                        time_2 = datetime.timedelta(hours= round(jamouts), minutes=menitoutx)
                        
                        if jamins < jamouts : 
                            recor.totaljam=time_2-time_1
                        elif jamouts < jamins :
                            recor.totaljam=time_1-time_2
                        elif jamins == jamouts :
                            recor.totaljam ="00:00:00"
            

        
    @api.depends('totalharga','harga','totaljam')
    def _def_count_harga(self):
        self.totalharga =0
        for record in self:
            if record.harga>0 :
                waktu   = record.totaljam.split(":")
                jam     = float(waktu[0])
                menit   = float(waktu[1])
                if jam>0 or menit>0 :
                    record.totalharga = float((jam*float(record.harga)))+float((menit/float(60))*float(record.harga))
                elif jam==0 or menit==0 :
                    record.totalharga = float(0)
