from email.policy import default
from odoo import api,fields,models
from odoo.exceptions import ValidationError


class hrms_bpjs(models.Model):
    _name = 'hrms.bpjs_master'
    _description = 'hrms.bpjs_master'

    name                        = fields.Char(string='Jumlah Tanggungan')
    bpjs_kesehatan_emp          = fields.Float(string=' Bpjs kesehatan (%)')
    bpjs_hari_tua               = fields.Float(string=' Jaminan hari tua (%)')
    bpjs_ketenagakerjaan        = fields.Float(string=' Bpjs Ketenagakerjaan (%)')
    bpjs_kesehatan_comp         = fields.Float(string=' Bpjs kesehatan (%)')
    bpjs_hari_tua_comp          = fields.Float(string=' Jaminan hari tua (%)')
    bpjs_ketenagakerjaan_comp   = fields.Float(string=' Bpjs Ketenagakerjaan (%)')


class master_bpjs_employee(models.Model):
    _name                   = 'hrms.bpjs_employee'
    _description            = 'hrms.bpjs_employee'

    employee_id                     = fields.Many2one(comodel_name='hr.employee',string="Karyawan")
    jumlah_tanggungan               = fields.Many2one(comodel_name='hrms.bpjs_master', string="Tanggungan")
    gaji_bpjs                       = fields.Char(string ="Gaji Bpjs:")
    no_bpjs_kesehatan               = fields.Char(string ="No Bpjs Kesehatan :")
    tarif_bpjs_kesehatan            = fields.Float(string=" Bpjs Kesehatan (%) :" ,default="0.00")
    tarif_bpjs_kesehatan_comp       = fields.Float(string=" Bpjs Kesehatan Perusahaan (%) :" ,default="0.00")
    no_bpjs_ketenagakerjaan         = fields.Char(string ="No Bpjs Ketenagakerjaan :")
    tarif_bpjs_ketenagakerjaan      = fields.Float(string="Bpjs Ketenagakerjaan(%) :" ,default="0.00")
    tarif_bpjs_ketenagakerjaan_comp = fields.Float(string="Bpjs Ketenagakerjaan Perusahaan (%):" ,default="0.00")
    no_jht                          = fields.Char(string ="No JHT :")
    tarif_jht                       = fields.Float(string="JHT (%):" ,default="0.00")
    tarif_jht_comp                  = fields.Float(string="JHT Perusahaan (%):" ,default="0.00")
   


    @api.onchange('jumlah_tanggungan')
    def _onchange_(self):
       for rec in self :
           rec.tarif_bpjs_kesehatan              =rec.jumlah_tanggungan.bpjs_kesehatan_emp
           rec.tarif_bpjs_ketenagakerjaan        =rec.jumlah_tanggungan.bpjs_ketenagakerjaan
           rec.tarif_jht                         =rec.jumlah_tanggungan.bpjs_hari_tua
           rec.tarif_bpjs_kesehatan_comp         =rec.jumlah_tanggungan.bpjs_kesehatan_comp
           rec.tarif_bpjs_ketenagakerjaan_comp   =rec.jumlah_tanggungan.bpjs_ketenagakerjaan_comp
           rec.tarif_jht_comp                    =rec.jumlah_tanggungan.bpjs_hari_tua_comp

    @api.onchange('employee_id')
    def cekbpjsemployee(self):
        countEmployee = self.getCountBpjsemployee(self.employee_id)
        if countEmployee > 0 :
            raise ValidationError("Data Karyawan sudah ada di master Bpjs")
    
    def getCountBpjsemployee(self,employee_id):
        result = self.env['hrms.bpjs_employee'].search_count([('employee_id','=',employee_id.id)])
        return result
