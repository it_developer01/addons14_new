
from email.policy import default
from unicodedata import name

from odoo.exceptions import UserError, ValidationError
from odoo import api ,fields ,models
class Payroll(models.Model):
    _name = 'hrms.payroll'
    _description ='hrms.payroll'

    #name         = fields.Char(String="Name")
    employee_id  = fields.Many2one(
        comodel_name='hr.employee',
        string      ='Nama Kariawan',
        )
    name = fields.Char(string='Payslip Name')
    noslip         = fields.Char(String="No slip")
    date_from      = fields.Date(String="Periode Gaji")
    ref            = fields.Char(String="Referensi")
    input_line_ids = fields.One2many(comodel_name='hrms.payroll.input_lines',inverse_name='payroll_id',String='input Lain-lain')
    workeddayline  = fields.One2many(comodel_name='hrms.payroll.working_days',inverse_name='payroll_id',String="Hari Kerja")
    date_to        = fields.Date(String="")
    contract_id    =  fields.Many2one(
        comodel_name='hr.contract',
        string      ='Nama Kariawan',
        )
    line_ids      =  fields.One2many(comodel_name='hrms.payroll.line_componen',inverse_name='payroll_id',String='Perhitungan gaji')   
    bpjs_ids      =  fields.One2many(comodel_name='hrms.bpjs.line_componen',inverse_name='payroll_id',String='Perhitungan Bpjs')   
    @api.depends('workeddayline')
    def isi(self):
        if self.workingdayline:
            code = self.workeddayline.code
            name = self.workeddayline.name

    @api.onchange('employee_id')
    def _onchange_employee_id(self):
                res    =[]
                result = self._getBpjs(self.employee_id.id)
                clause  =[('employee_id','=',self.employee_id.id)]
                rescom = self.env['hrms.master_salary'].search(clause)
                
                if result :
                    tarifkesehatanRp         = self.potonganBpjs(result.gaji_bpjs,result.tarif_bpjs_kesehatan)
                    tarifketenagakerjaanRp   = self.potonganBpjs(result.gaji_bpjs,result.tarif_bpjs_ketenagakerjaan)
                    tarif_jht_rp             = self.potonganBpjs(result.gaji_bpjs,result.tarif_jht)
                    totalpotonganbpjs        = self.sumPotonganBpjs(tarifkesehatanRp,tarifketenagakerjaanRp,tarif_jht_rp)
                    self.bpjs_ids = [(0,0,{
                        'gajibpjs'                    :result.gaji_bpjs,
                        'bpjs_kesehatan'              :result.tarif_bpjs_kesehatan,
                        'bpjs_ketenagakerjaan'        :result.tarif_bpjs_ketenagakerjaan,
                        'bpjs_jht'                    :result.tarif_jht,
                        'bpjs_kesehatan_rp'           :tarifkesehatanRp,
                        'bpjs_ketenagakerjaan_rp'     :tarifketenagakerjaanRp,
                        'bpjs_jht_rp'                 :tarif_jht_rp,
                        'total_potongan'              :totalpotonganbpjs
                     
                        }
                    )]
                
                if rescom :
                    clause    = [('id','in',rescom.componen_id.ids)]
                    execute   = self.env['hrms.master_pay_line_component'].search(clause)
                    ax        =[]
                    for a in execute:
                        self.line_ids = [(0,0,{
                        'name'                    :a['name'],
                        'code'                    :a['name'],
                        'component'               :a['componen_id'],
                        'quantity'                :a['quantity'],
                        'rate'                    :a['rate'],
                        'amount'                  :a['amount'],
                        'total'                   :float(a['amount'])*float(a['quantity'])
                     
                        }
                    )]   
                        #ax.append(a['name'])
                    #self.name =ax  
                   
                   
    def _getBpjs(self,employeid):
         clause1 = [('employee_id','=',employeid)]
         return   self.env['hrms.bpjs_employee'].search(clause1)


    def potonganBpjs(self,gajibpjs,tarifkesehatan):
        totalPotongan = float(float(tarifkesehatan)/100)*float(gajibpjs)
        return totalPotongan

    def sumPotonganBpjs(self,bpjskesehatan,bpjsketenagakerjaan,bpjsjht):
        sumpotongan   = float(bpjskesehatan+bpjsketenagakerjaan+bpjsjht)
        return sumpotongan


          #for rec in self :
              

           # if result :
           #     self.ref  = result.ids
            #    attendances = {
             #   'name': _("Normal Working Days paid at 100%"),
            #    'sequence': 1,
              #  'code': 'WORK100',
              #  'number_of_days': result['id'],
              #  'number_of_hours': result['hours'],
              #  'contract_id': result.id,
               # }

           # res.append(attendances)
           # res.extend(leaves.values())
       

class worked_days(models.Model):
    _name        ='hrms.payroll.working_days'
    _description ='hrms.paryoll.working_days'

    payroll_id   = fields.Many2one(
                    string='payroll id',
                    comodel_name='hrms.payroll',
                    
    )
   
    code         = fields.Char(
        string   ='Kode',
    )
    sequence        = fields.Integer(required=True, index=True, default=10)
    name            = fields.Char(string='Description', required=True)
    number_of_days  = fields.Float(string='Number of Days')
    number_of_hours = fields.Float(string='Number of Hours')
    
class inputlines(models.Model):
    _name           ='hrms.payroll.input_lines'
    _description    ='hrms.payroll.input_lines'
    _order          ='payroll_id, sequence'

    payroll_id      =fields.Many2one(comodel_name='hrms.payroll', string='Pay Slip', required=True, ondelete='cascade', index=True)
    name            =fields.Char(String='description',required=True)
    code            =fields.Char(String='code',required=True)
    sequence        =fields.Integer(required=True, index=True, default=10)
    amount          =fields.Float(help="It is used in computation. For e.g. A rule for sales having "
                               "1% commission of basic salary for per product can defined in expression "
                               "like result = inputs.SALEURO.amount * contract.wage*0.01.")
    contract_id     = fields.Many2one('hr.contract', string='Contract', required=True,
        help="The contract for which applied this input")
        
class rulePayroll(models.Model):
      _name         ='hrms.payroll.line_componen'
      _description  ='hrms.payroll.line_componen'
      _order        ='payroll_id'


      payroll_id    = fields.Many2one('hrms.payroll','Payroll id') 
      name          = fields.Char('Description') 
      code          = fields.Char('Kode')
      #employee_id   = fields.Many2one('hr.employee', string='Employee', required=True)
      #contract_id   = fields.Many2one('hr.contract', string='Contract', required=True, index=True)
      component     = fields.Many2one('hrms.payroll_component', string='Componen', required=True)
      rate          = fields.Float(string='Rate (%)', default=100.0)
      amount        = fields.Float()
      quantity      = fields.Float(default=1.0)
      total         = fields.Float(compute='_compute_total', string='Total')

      @api.depends('quantity', 'amount', 'rate')
      def _compute_total(self):
        for line in self:
            line.total = float(line.quantity) * line.amount * line.rate / 100
            
class PayrollComponent(models.Model):
      _name           ='hrms.payroll_component'
      _description    ='hrms.payroll_component'


      name            =fields.Char(string="name")
      description     =fields.Char(string="description")
      component_id    =fields.Many2one(comodel_name='hrms.master_salary',String='Komponen id')
 
class hrms_masterSalary(models.Model):
     _name                   ='hrms.master_salary'
     _description            ='hrms.master_salary'

     name                    = fields.Char(
         string='Kode Referensi',
         required=True
     )
     employee_id            = fields.Many2one( 
         comodel_name='hr.employee',
         string='Kariawan'
                        
     )
     kontrak            = fields.Many2one( 
         comodel_name   ='hr.contract',
         string         ='Kontrak'
                        
     )
    
     componen_id     = fields.One2many(
         string      ='komponen',
         comodel_name='hrms.master_pay_line_component',
         inverse_name='com_id'
     )
     grandtotal   =fields.Float(string="Total Bruto")
      
     

     class MasterPaycomponent(models.Model):
         _name        = 'hrms.master_pay_line_component'
         _description = 'hrms.master_pay_line_component'

         
         name         =fields.Char(string='Name')
         description  =fields.Char(string='Description')
         amount       =fields.Float(string="Jumlah")
         total        =fields.Float(string="Total",compute='_compute_total')
         rate         =fields.Float(string='Rate (%)', default=100.0)
         quantity     =fields.Float(default=1.0,string='Kuantitas')
         componen_id  = fields.Many2one(
         string       ='komponen',
         comodel_name ='hrms.payroll_component'
       
         )

         
         com_id        =fields.Many2one(
             string      ='kom_id',
             comodel_name='hrms.master_salary'
         )
        
         @api.depends('quantity', 'amount', 'rate')
         def _compute_total(self):
            for line in self:
                line.total = float(line.quantity) * line.amount * line.rate / 100
    
     class BpjsPayroll(models.Model):
        _name           ='hrms.bpjs.line_componen'
        _description    ='hrms.bpjs.line_componen'


        noreferensi             =fields.Char(string="No refensi")
        payroll_id              =fields.Many2one(comodel_name='hrms.payroll')
        gajibpjs                =fields.Float(default=0,string="Gaji Bpjs")
        bpjs_kesehatan          =fields.Float(default=0,string="Bpjs Kesehatan %")
        bpjs_ketenagakerjaan    =fields.Float(default=0,string="Bpjs Ketenagakerjaan %")
        bpjs_jht                =fields.Float(default=0,string="Jaminan Hari tua %")
        bpjs_ketenagakerjaan_rp =fields.Float(default=0,string="Potongan Bpjs Ketenagakerjaan (Rp)")
        bpjs_kesehatan_rp       =fields.Float(default=0,string="Potongan Bpjs Kesehatan (Rp)")
        bpjs_jht_rp             =fields.Float(default=0,string="Potongan Jaminan Hari tua (Rp)")
        total_potongan          =fields.Float(default=0,string="Total Potongan (Rp)")


        